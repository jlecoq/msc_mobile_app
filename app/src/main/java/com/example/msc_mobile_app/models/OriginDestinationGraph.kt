package com.example.msc_mobile_app.models

import com.example.msc_mobile_app.APIs.DistanceMatrixApi
import com.example.msc_mobile_app.extensions.copy
import com.example.msc_mobile_app.utils.Callback2
import com.example.msc_mobile_app.utils.Callback3
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.runBlocking

data class Path(
    var duration: Int = 0,
    var distance: Int = 0,
    var intermediaryNodeIndex: Int? = null)

data class Itinerary(val origin: LatLng, val destination: LatLng, val steps: NodeLocations)

typealias Matrix<T> = Array<Array<T>>
typealias MatrixIndices = Array<Int>
typealias NodesLocations = Map<Int, Itinerary>
typealias NodeLocations = ArrayList<LatLng>
typealias CostFunction = Callback3<Int, Int, MatrixIndices, Array<Int>>

object OriginDestinationGraph {
    lateinit var pointOfInterests: Array<Poi>
    lateinit var connectedGraphMatrices: Matrix<Path?>
    lateinit var meetAtEarliestMatrices: Matrix<Path?>
    lateinit var meetAtClosestMatrices: Matrix<Path?>

    lateinit var destination: Poi
    var destinationDurationClosest = Int.MAX_VALUE
    var destinationDistanceClosest = Int.MAX_VALUE

    var destinationDurationEarliest = Int.MAX_VALUE
    var destinationDistanceEarliest = Int.MAX_VALUE

    lateinit var nodesLocationsToMeetAtEarliest: NodesLocations
    lateinit var nodesLocationsToMeetAtClosest: NodesLocations

    var computeMeetAtClosestDone = false
    var computeMeetAtEarliestDone = false
    var transitiveClosureDone = false
    var connectedGraphDone = false

    fun init(pointOfInterests: Array<Poi>) {
        this.pointOfInterests = pointOfInterests
        connectedGraphMatrices = Array(pointOfInterests.size) { Array(pointOfInterests.size) { null } }
    }

    fun buildRandomConnectedGraph() {
        connectedGraphMatrices = Array(pointOfInterests.size) { Array(pointOfInterests.size) { null } }
        val requests = ArrayList<Deferred<Path?>>()
        val indicesInMatrix = ArrayList<Array<Int>>()
        var alreadyLinkedIndex = 0

        runBlocking {
            for (i in connectedGraphMatrices.indices) {
                if (i == 0) {
                    val randomIndex = (connectedGraphMatrices.indices).random()
                    alreadyLinkedIndex = randomIndex
                    requests.add(async { DistanceMatrixApi.path(pointOfInterests[i].location, pointOfInterests[randomIndex].location) })
                    indicesInMatrix.add(arrayOf(i, randomIndex))

                    requests.add(async { DistanceMatrixApi.path(pointOfInterests[randomIndex].location, pointOfInterests[i].location) })
                    indicesInMatrix.add(arrayOf(randomIndex, i))

                }
                else {
                    val randomIndex = (0 until i).random()

                    if (i == alreadyLinkedIndex) {
                        continue
                    }

                    requests.add(async { DistanceMatrixApi.path(pointOfInterests[i].location, pointOfInterests[randomIndex].location) })
                    indicesInMatrix.add(arrayOf(i, randomIndex))

                    requests.add(async { DistanceMatrixApi.path(pointOfInterests[randomIndex].location, pointOfInterests[i].location) })
                    indicesInMatrix.add(arrayOf(randomIndex, i))
                }
            }

            val pathResults = requests.awaitAll()

            for (i in pathResults.indices) {
                val indices = indicesInMatrix[i]
                connectedGraphMatrices[indices[0]][indices[1]] = pathResults[i]
            }
        }

        connectedGraphDone = true
        if (transitiveClosureDone) {
            transitiveClosureDone = false
            computeMeetAtEarliestDone = false
            computeMeetAtClosestDone = false
        }
    }

    fun buildTransitiveClosure() {
        buildMeetAtEarliestTransitiveClosure()
        buildMeetAtClosestTransitiveClosure()
        transitiveClosureDone = true
    }

    fun buildMeetAtClosestTransitiveClosure() {
        meetAtClosestMatrices = connectedGraphMatrices.copy()

        for (k in meetAtClosestMatrices.indices) {
            for (i in meetAtClosestMatrices.indices) {
                for (j in meetAtClosestMatrices.indices) {
                    if (meetAtClosestMatrices[i][j] == null && (meetAtClosestMatrices[i][k] != null && meetAtClosestMatrices[k][j] != null)) {
                        val distance = meetAtClosestMatrices[i][k]!!.distance + meetAtClosestMatrices[k][j]!!.distance
                        val duration = meetAtClosestMatrices[i][k]!!.duration + meetAtClosestMatrices[k][j]!!.duration
                        meetAtClosestMatrices[i][j] = Path(duration, distance, k)
                    }
                    else if (meetAtClosestMatrices[i][j] != null && (meetAtClosestMatrices[i][k] != null && meetAtClosestMatrices[k][j] != null)) {
                        if (meetAtClosestMatrices[i][j]!!.distance > (meetAtClosestMatrices[i][k]!!.distance + meetAtClosestMatrices[k][j]!!.distance)) {
                            val duration = meetAtClosestMatrices[i][k]!!.duration + meetAtClosestMatrices[k][j]!!.duration
                            val distance = meetAtClosestMatrices[i][k]!!.distance + meetAtClosestMatrices[k][j]!!.distance
                            meetAtClosestMatrices[i][j] = Path(duration, distance, k)
                        }
                    }
                }
            }
        }
    }

    fun buildMeetAtEarliestTransitiveClosure() {
        meetAtEarliestMatrices = connectedGraphMatrices.copy()

        for (k in meetAtEarliestMatrices.indices) {
            for (i in meetAtEarliestMatrices.indices) {
                for (j in meetAtEarliestMatrices.indices) {
                    if (meetAtEarliestMatrices[i][j] == null && (meetAtEarliestMatrices[i][k] != null && meetAtEarliestMatrices[k][j] != null)) {
                        val duration = meetAtEarliestMatrices[i][k]!!.duration + meetAtEarliestMatrices[k][j]!!.duration
                        val distance = meetAtEarliestMatrices[i][k]!!.distance + meetAtEarliestMatrices[k][j]!!.distance
                        meetAtEarliestMatrices[i][j] = Path(duration, distance, k)
                    }
                    else if (meetAtEarliestMatrices[i][j] != null && (meetAtEarliestMatrices[i][k] != null && meetAtEarliestMatrices[k][j] != null)) {
                        if (meetAtEarliestMatrices[i][j]!!.duration > (meetAtEarliestMatrices[i][k]!!.duration + meetAtEarliestMatrices[k][j]!!.duration)) {
                            val duration = meetAtEarliestMatrices[i][k]!!.duration + meetAtEarliestMatrices[k][j]!!.duration
                            val distance = meetAtEarliestMatrices[i][k]!!.distance + meetAtEarliestMatrices[k][j]!!.distance
                            meetAtEarliestMatrices[i][j] = Path(duration, distance, k)
                        }
                    }
                }
            }
        }
    }

    private fun poiIndex(costFunction: CostFunction, updateCost: Callback2<Int, Int>): Int {
        var finalMaxValue = Int.MAX_VALUE
        var poiIndex = Int.MAX_VALUE
        val studentsIndices = studentsIndices()

        for (i in pointOfInterests.indices) {
            if (pointOfInterests[i].type == Type.PERSON) {
                continue
            }

            var maxValueBetweenStudents = Int.MIN_VALUE
            var studentIdx = Int.MIN_VALUE
            var result: Array<Int>
            for (studentIndex in studentsIndices) {
//                maxValueBetweenStudents, studentIdx = costFunction(maxValueBetweenStudents, maxIndex, arrayOf(studentIndex, i))
                result = costFunction(maxValueBetweenStudents, studentIdx, arrayOf(studentIndex, i))
                maxValueBetweenStudents = result[0]
                studentIdx = result[1]
            }

            if (maxValueBetweenStudents < finalMaxValue) {
                updateCost(studentIdx, i)
                finalMaxValue = maxValueBetweenStudents
                poiIndex = i
            }
        }

        return poiIndex
    }

    fun computeMeetAtEarliest() {
        val costFunction = { maxDurationBetweenStudents: Int, maxIndex: Int, matrixIndices: MatrixIndices ->
            var studentIndex = matrixIndices[0]
            val destinationIndex = matrixIndices[1]
            var maxDurationBetweenStudents = maxDurationBetweenStudents
            var maxIndex = maxIndex
            if (maxDurationBetweenStudents < meetAtEarliestMatrices[studentIndex][destinationIndex]!!.duration) {
                maxDurationBetweenStudents = meetAtEarliestMatrices[studentIndex][destinationIndex]!!.duration
                maxIndex = studentIndex
            }
            arrayOf(maxDurationBetweenStudents, maxIndex)
        }

        val updateCost = { i: Int, j: Int ->
            destinationDistanceEarliest = meetAtEarliestMatrices[i][j]!!.distance
            destinationDurationEarliest = meetAtEarliestMatrices[i][j]!!.duration
        }

        nodesLocationsToMeetAtEarliest = nodesLocationToMeet(costFunction, updateCost, meetAtEarliestMatrices as Matrix<Path?>)
        computeMeetAtEarliestDone = true
    }

    fun computeMeetAtClosest() {
        val costFunction = { maxDistanceBetweenStudents: Int, maxIndex: Int, matrixIndices: MatrixIndices ->
            val studentIndex = matrixIndices[0]
            val destinationIndex = matrixIndices[1]
            var maxDistanceBetweenStudents = maxDistanceBetweenStudents
            var maxIndex = maxIndex
            if (maxDistanceBetweenStudents < meetAtClosestMatrices[studentIndex][destinationIndex]!!.distance) {
                maxDistanceBetweenStudents = meetAtClosestMatrices[studentIndex][destinationIndex]!!.distance
                maxIndex = studentIndex
            }
            arrayOf(maxDistanceBetweenStudents, maxIndex)
        }

        val updateCost = { i: Int, j: Int ->
            destinationDistanceClosest = meetAtClosestMatrices[i][j]!!.distance
            destinationDurationClosest = meetAtClosestMatrices[i][j]!!.duration
        }

        nodesLocationsToMeetAtClosest = nodesLocationToMeet(costFunction, updateCost, meetAtClosestMatrices as Matrix<Path?>)
        computeMeetAtClosestDone = true
    }

    fun nodesLocationToMeet(costFunction: CostFunction, updateCost: Callback2<Int, Int>, meetMatrix: Matrix<Path?>): NodesLocations {
        val poiIndex = poiIndex(costFunction, updateCost)
        destination = pointOfInterests[poiIndex]
        val studentsIndices = studentsIndices()
        val nodesLocation = mutableMapOf<Int, Itinerary>()

        // For each student get the node(s) it should go through
        for (i in studentsIndices.indices) {
            nodesLocation[studentsIndices[i]] = shortestPath(studentsIndices[i], poiIndex, meetMatrix)
        }

        return nodesLocation
    }

    private fun shortestPath(sourceIndex: Int, destinationIndex: Int, meetMatrix: Matrix<Path?>): Itinerary {
        var destinationIndex = destinationIndex
        val finalIndex = destinationIndex
        var destNode = meetMatrix[sourceIndex][destinationIndex]

        val origin = pointOfInterests[sourceIndex].location
        val destination = pointOfInterests[finalIndex].location

        val steps = arrayListOf<LatLng>()

        while (destNode!!.intermediaryNodeIndex != null) {
            destinationIndex = destNode.intermediaryNodeIndex!!
            steps.add(pointOfInterests[destinationIndex].location)
            destNode = meetMatrix[destinationIndex][finalIndex]
        }

        return Itinerary(origin, destination, steps)
    }

    private fun studentsIndices(): ArrayList<Int> {
        val studentsIndices = ArrayList<Int>()

        for (i in pointOfInterests.indices) {
            if (pointOfInterests[i].type == Type.PERSON) {
                studentsIndices.add(i)
            }
        }

        return studentsIndices
    }
}

/*
while (true) {
    val randomIndex = (connectedGraphMatrix.indices).random()
    val predecessors =
        OriginDestinationGraph.predecessor(OriginDestinationGraph.connectedGraphMatrix, i)

    if ((randomIndex != i) && predecessors[randomIndex] != true) {
        runBlocking {
            OriginDestinationGraph.connectedGraphMatrix[i][randomIndex] = DistanceMatrixApi.path(
                OriginDestinationGraph.pointOfInterests[i].location, OriginDestinationGraph.pointOfInterests[randomIndex].location)
            OriginDestinationGraph.connectedGraphMatrix[randomIndex][i] = DistanceMatrixApi.path(
                OriginDestinationGraph.pointOfInterests[randomIndex].location, OriginDestinationGraph.pointOfInterests[i].location)
        }
        break
    }

    connectedEdges++
}*/

/*

// transitiveClosureMatrix = Array(pointOfInterests.size) { Array(pointOfInterests.size) { null } }
*/

/*

    fun shortestPoiIndex(): Int {
        var finalMaxDistance = Int.MAX_VALUE
        var poiIndex = Int.MAX_VALUE
        var studentsIndices = studentsIndices()

        for (i in meetAtClosestMatrix.indices) {
            if (pointOfInterests[i].type == Type.PERSON) {
                continue
            }

            var maxDistanceBetweenStudents = Int.MIN_VALUE
            for (studentIndex in studentsIndices) {
                if (maxDistanceBetweenStudents < meetAtClosestMatrix[studentIndex][i]!!.distance) {
                    maxDistanceBetweenStudents = meetAtClosestMatrix[studentIndex][i]!!.distance
                }
            }

            if (maxDistanceBetweenStudents < finalMaxDistance) {
                finalMaxDistance = maxDistanceBetweenStudents
                poiIndex = i
            }
        }

        return poiIndex
    }

    fun nodesLocationToMeetAtShortest2(): Array<ArrayList<LatLng>> {
        var poiIndex = shortestPoiIndex()
        val studentsIndices = studentsIndices()
        val nodesLocation = Array(studentsIndices.size) { ArrayList<LatLng>() }

        // For each student get the node(s) it should go through
        for (i in studentsIndices.indices) {
            Log.e(javaClass.simpleName, studentsIndices[i].toString())
            nodesLocation[i] = pathForShortestDistance(studentsIndices[i], poiIndex)
        }

        return nodesLocation
    }

    private fun pathForShortestDistance(sourceIndex: Int, destinationIndex: Int): ArrayList<LatLng> {
        val pathLocations = ArrayList<LatLng>()
        var destinationIndex = destinationIndex
        val finalIndex = destinationIndex
        var destNode = meetAtClosestMatrix[sourceIndex][destinationIndex]

        pathLocations.add(pointOfInterests[sourceIndex].location)

        while (destNode!!.intermediaryNodeIndex != null) {
            destinationIndex = destNode.intermediaryNodeIndex!!
            pathLocations.add(pointOfInterests[destinationIndex].location)
            destNode = meetAtClosestMatrix[destinationIndex][finalIndex]
        }

        pathLocations.add(pointOfInterests[finalIndex].location)
        return pathLocations
    }



 */

/*
fun predecessor(matrix: Array<Array<Path?>>, nodeIndex: Int): Map<Int, Boolean> {
    val predecessors = mutableMapOf<Int, Boolean>()

    for (i in matrix.indices) {
        if ((nodeIndex != i) && (matrix[i][nodeIndex] != null)) {
            predecessors[i] = true
        }
    }

    return predecessors
}*/

/*
fun buildTransitiveClosure() {
    transitiveClosureMatrix = connectedGraphMatrix.copy()

    // TODO: To improve?
    // TODO: 2 matrix, 1 which optimize distances, 1 which optimize durations?

    for (k in transitiveClosureMatrix.indices) {
        for (i in transitiveClosureMatrix.indices) {
            for (j in transitiveClosureMatrix.indices) {
                if (transitiveClosureMatrix[i][j] == null && (transitiveClosureMatrix[i][k] != null && transitiveClosureMatrix[k][j] != null)) {
                    val distance = transitiveClosureMatrix[i][k]!!.distance + transitiveClosureMatrix[k][j]!!.distance
                    val duration = transitiveClosureMatrix[i][k]!!.duration + transitiveClosureMatrix[k][j]!!.duration
                    val intermediaryNodeIndex = k

                    transitiveClosureMatrix[i][j] = Path(duration, distance, intermediaryNodeIndex)
                }
                else if (transitiveClosureMatrix[i][j] != null && (transitiveClosureMatrix[i][k] != null && transitiveClosureMatrix[k][j] != null)) {
                    if (transitiveClosureMatrix[i][j]!!.distance > (transitiveClosureMatrix[i][k]!!.distance + transitiveClosureMatrix[k][j]!!.distance)) {
                        transitiveClosureMatrix[i][j]!!.intermediaryNodeIndex = k
                        transitiveClosureMatrix[i][j]!!.distance = transitiveClosureMatrix[i][k]!!.distance + transitiveClosureMatrix[k][j]!!.distance
                        transitiveClosureMatrix[i][j]!!.duration = transitiveClosureMatrix[i][k]!!.duration + transitiveClosureMatrix[k][j]!!.duration
                    }
                }
            }
        }
    }
    transitiveClosureDone = true
}*/


/*

object OriginDestinationGraph {
    lateinit var pointOfInterests: Array<Poi>
    lateinit var connectedGraphMatrix: Matrix<GenericPath?>
    lateinit var meetAtEarliestMatrix: Matrix<GenericPath?>
    lateinit var meetAtClosestMatrix: Matrix<GenericPath?>

    lateinit var destination: Poi
    var destinationDistance = Int.MAX_VALUE
    var destinationDuration = Int.MAX_VALUE

    lateinit var nodesLocationsToMeetAtEarliest: NodesLocations
    lateinit var nodesLocationsToMeetAtClosest: NodesLocations

    var computeMeetAtClosestDone = false
    var computeMeetAtEarliestDone = false
    var transitiveClosureDone = false
    var connectedGraphDone = false

    fun init(pointOfInterests: Array<Poi>) {
        this.pointOfInterests = pointOfInterests
        connectedGraphMatrix = Array(pointOfInterests.size) { Array(pointOfInterests.size) { null } }
    }

    fun buildRandomConnectedGraph() {
        connectedGraphMatrix = Array(pointOfInterests.size) { Array(pointOfInterests.size) { null } }
        val requests = ArrayList<Deferred<GenericPath?>>()
        val indicesInMatrix = ArrayList<Array<Int>>()
        var alreadyLinkedIndex = 0

        runBlocking {
            for (i in connectedGraphMatrix.indices) {
                if (i == 0) {
                    val randomIndex = (connectedGraphMatrix.indices).random()
                    alreadyLinkedIndex = randomIndex
                    requests.add(async { DistanceMatrixApi.path(pointOfInterests[i].location, pointOfInterests[randomIndex].location) })
                    indicesInMatrix.add(arrayOf(i, randomIndex))

                    requests.add(async { DistanceMatrixApi.path(pointOfInterests[randomIndex].location, pointOfInterests[i].location) })
                    indicesInMatrix.add(arrayOf(randomIndex, i))

                }
                else {
                    val randomIndex = (0 until i).random()

                    if (i == alreadyLinkedIndex) {
                        continue
                    }

                    requests.add(async { DistanceMatrixApi.path(pointOfInterests[i].location, pointOfInterests[randomIndex].location) })
                    indicesInMatrix.add(arrayOf(i, randomIndex))

                    requests.add(async { DistanceMatrixApi.path(pointOfInterests[randomIndex].location, pointOfInterests[i].location) })
                    indicesInMatrix.add(arrayOf(randomIndex, i))
                }
            }

            val pathResults = requests.awaitAll()

            for (i in pathResults.indices) {
                val indices = indicesInMatrix[i]
                connectedGraphMatrix[indices[0]][indices[1]] = pathResults[i]
            }
        }

        connectedGraphDone = true
        if (transitiveClosureDone) {
            transitiveClosureDone = false
        }
    }

    private fun copyToDurationPath(connectedGraphMatrices: Matrix<GenericPath?>): Matrix<DurationPath?> {
        val meetAtEarliestMatrix: Matrix<DurationPath?> = Array(pointOfInterests.size) { Array(pointOfInterests.size) { null } }

        for (i in connectedGraphMatrices.indices) {
            for (j in connectedGraphMatrices.indices) {
                if (connectedGraphMatrices[i][j] == null) {
                    meetAtEarliestMatrix[i][j] = null
                } else {
                    meetAtEarliestMatrix[i][j] = DurationPath(connectedGraphMatrices[i][j]!!.duration, connectedGraphMatrices[i][j]!!.intermediaryNodeIndex)
                }
            }
        }

        return meetAtEarliestMatrix
    }

    private fun copyToDistancePath(connectedGraphMatrices: Matrix<GenericPath?>): Matrix<DistancePath?> {
        val meetAtShortestMatrix: Matrix<DistancePath?> = Array(pointOfInterests.size) { Array(pointOfInterests.size) { null } }

        for (i in connectedGraphMatrices.indices) {
            for (j in connectedGraphMatrices.indices) {
                if (connectedGraphMatrices[i][j] == null) {
                    meetAtShortestMatrix[i][j] = null
                } else {
                    meetAtShortestMatrix[i][j] = DistancePath(connectedGraphMatrices[i][j]!!.distance, connectedGraphMatrices[i][j]!!.intermediaryNodeIndex)
                }
            }
        }

        return meetAtShortestMatrix
    }

    fun buildTransitiveClosure() {
        buildMeetAtEarliestTransitiveClosure()
        buildMeetAtClosestTransitiveClosure()
        transitiveClosureDone = true
    }

    fun buildMeetAtClosestTransitiveClosure() {
        meetAtClosestMatrix = copyToDistancePath(connectedGraphMatrix)

        for (k in meetAtClosestMatrix.indices) {
            for (i in meetAtClosestMatrix.indices) {
                for (j in meetAtClosestMatrix.indices) {
                    if (meetAtClosestMatrix[i][j] == null && (meetAtClosestMatrix[i][k] != null && meetAtClosestMatrix[k][j] != null)) {
                        val distance = meetAtClosestMatrix[i][k]!!.distance + meetAtClosestMatrix[k][j]!!.distance
                        meetAtClosestMatrix[i][j] = DistancePath(distance, k)
                    }
                    else if (meetAtClosestMatrix[i][j] != null && (meetAtClosestMatrix[i][k] != null && meetAtClosestMatrix[k][j] != null)) {
                        if (meetAtClosestMatrix[i][j]!!.distance > (meetAtClosestMatrix[i][k]!!.distance + meetAtClosestMatrix[k][j]!!.distance)) {
                            meetAtClosestMatrix[i][j]!!.intermediaryNodeIndex = k
                            meetAtClosestMatrix[i][j]!!.distance = meetAtClosestMatrix[i][k]!!.distance + meetAtClosestMatrix[k][j]!!.distance
                        }
                    }
                }
            }
        }
    }

    fun buildMeetAtEarliestTransitiveClosure() {
        meetAtEarliestMatrix = copyToDurationPath(connectedGraphMatrix)

        for (k in meetAtEarliestMatrix.indices) {
            for (i in meetAtEarliestMatrix.indices) {
                for (j in meetAtEarliestMatrix.indices) {
                    if (meetAtEarliestMatrix[i][j] == null && (meetAtEarliestMatrix[i][k] != null && meetAtEarliestMatrix[k][j] != null)) {
                        val duration = meetAtEarliestMatrix[i][k]!!.duration + meetAtEarliestMatrix[k][j]!!.duration
                        meetAtEarliestMatrix[i][j] = DurationPath(duration, k)
                    }
                    else if (meetAtEarliestMatrix[i][j] != null && (meetAtEarliestMatrix[i][k] != null && meetAtEarliestMatrix[k][j] != null)) {
                        if (meetAtEarliestMatrix[i][j]!!.duration > (meetAtEarliestMatrix[i][k]!!.duration + meetAtEarliestMatrix[k][j]!!.duration)) {
                            meetAtEarliestMatrix[i][j]!!.intermediaryNodeIndex = k
                            meetAtEarliestMatrix[i][j]!!.duration = meetAtEarliestMatrix[i][k]!!.duration + meetAtEarliestMatrix[k][j]!!.duration
                        }
                    }
                }
            }
        }
    }

    private fun poiIndex(costFunction: CostFunction, updateCost: Callback<Int>): Int {
        var finalMaxValue = Int.MAX_VALUE
        var poiIndex = Int.MAX_VALUE
        val studentsIndices = studentsIndices()

        for (i in pointOfInterests.indices) {
            if (pointOfInterests[i].type == Type.PERSON) {
                continue
            }

            var maxValueBetweenStudents = Int.MIN_VALUE
            for (studentIndex in studentsIndices) {
                maxValueBetweenStudents = costFunction(maxValueBetweenStudents, arrayOf(studentIndex, i))
            }

            if (maxValueBetweenStudents < finalMaxValue) {
                finalMaxValue = maxValueBetweenStudents
                poiIndex = i
            }
        }

        return poiIndex
    }

    fun computeMeetAtEarliest() {
        val costFunction = { maxDurationBetweenStudents: Int, matrixIndices: MatrixIndices ->
            val studentIndex = matrixIndices[0]
            val destinationIndex = matrixIndices[1]
            var maxDurationBetweenStudents = maxDurationBetweenStudents
            if (maxDurationBetweenStudents < meetAtEarliestMatrix[studentIndex][destinationIndex]!!.duration) {
                maxDurationBetweenStudents = meetAtEarliestMatrix[studentIndex][destinationIndex]!!.duration
            }
            maxDurationBetweenStudents
        }

        val updateCost = {

        }

        nodesLocationsToMeetAtEarliest = nodesLocationToMeet(costFunction, meetAtEarliestMatrix as Matrix<Path?>)
        computeMeetAtEarliestDone = true
    }

    fun computeMeetAtClosest() {
        val costFunction = { maxDistanceBetweenStudents: Int, matrixIndices: MatrixIndices ->
            val studentIndex = matrixIndices[0]
            val destinationIndex = matrixIndices[1]
            var maxDistanceBetweenStudents = maxDistanceBetweenStudents
            if (maxDistanceBetweenStudents < meetAtClosestMatrix[studentIndex][destinationIndex]!!.distance) {
                maxDistanceBetweenStudents = meetAtClosestMatrix[studentIndex][destinationIndex]!!.distance
            }
            maxDistanceBetweenStudents
        }

        nodesLocationsToMeetAtClosest = nodesLocationToMeet(costFunction, meetAtClosestMatrix as Matrix<Path?>)
        computeMeetAtClosestDone = true
    }

    fun nodesLocationToMeet(costFunction: Callback2<Int, MatrixIndices, Int>, meetMatrix: Matrix<Path?>): NodesLocations {
        val poiIndex = poiIndex(costFunction)
        destination = pointOfInterests[poiIndex]
        val studentsIndices = studentsIndices()
        val nodesLocation = mutableMapOf<Int, Itinerary>()

        // For each student get the node(s) it should go through
        for (i in studentsIndices.indices) {
            nodesLocation[studentsIndices[i]] = shortestPath(studentsIndices[i], poiIndex, meetMatrix)
        }

        return nodesLocation
    }

    private fun shortestPath(sourceIndex: Int, destinationIndex: Int, meetMatrix: Matrix<Path?>): Itinerary {
        var destinationIndex = destinationIndex
        val finalIndex = destinationIndex
        var destNode = meetMatrix[sourceIndex][destinationIndex]

        val origin = pointOfInterests[sourceIndex].location
        val destination = pointOfInterests[finalIndex].location

        val steps = arrayListOf<LatLng>()

        while (destNode!!.intermediaryNodeIndex != null) {
            destinationIndex = destNode.intermediaryNodeIndex!!
            steps.add(pointOfInterests[destinationIndex].location)
            destNode = meetMatrix[destinationIndex][finalIndex]
        }

        return Itinerary(origin, destination, steps)
    }

    private fun studentsIndices(): ArrayList<Int> {
        val studentsIndices = ArrayList<Int>()

        for (i in pointOfInterests.indices) {
            if (pointOfInterests[i].type == Type.PERSON) {
                studentsIndices.add(i)
            }
        }

        return studentsIndices
    }
}
 */


/*

data class DurationPath(var duration: Int = 0,
                        override var intermediaryNodeIndex: Int? = null): Path(intermediaryNodeIndex)

data class DistancePath(var distance: Int = 0,
                        override var intermediaryNodeIndex: Int? = null): Path(intermediaryNodeIndex)


    private fun copyToDurationPath(connectedGraphMatrices: Matrix<GenericPath?>): Matrix<DurationPath?> {
        val meetAtEarliestMatrix: Matrix<DurationPath?> = Array(pointOfInterests.size) { Array(pointOfInterests.size) { null } }

        for (i in connectedGraphMatrices.indices) {
            for (j in connectedGraphMatrices.indices) {
                if (connectedGraphMatrices[i][j] == null) {
                    meetAtEarliestMatrix[i][j] = null
                } else {
                    meetAtEarliestMatrix[i][j] = DurationPath(connectedGraphMatrices[i][j]!!.duration, connectedGraphMatrices[i][j]!!.intermediaryNodeIndex)
                }
            }
        }

        return meetAtEarliestMatrix
    }

    private fun copyToDistancePath(connectedGraphMatrices: Matrix<GenericPath?>): Matrix<DistancePath?> {
        val meetAtShortestMatrix: Matrix<DistancePath?> = Array(pointOfInterests.size) { Array(pointOfInterests.size) { null } }

        for (i in connectedGraphMatrices.indices) {
            for (j in connectedGraphMatrices.indices) {
                if (connectedGraphMatrices[i][j] == null) {
                    meetAtShortestMatrix[i][j] = null
                } else {
                    meetAtShortestMatrix[i][j] = DistancePath(connectedGraphMatrices[i][j]!!.distance, connectedGraphMatrices[i][j]!!.intermediaryNodeIndex)
                }
            }
        }

        return meetAtShortestMatrix
    }

 */