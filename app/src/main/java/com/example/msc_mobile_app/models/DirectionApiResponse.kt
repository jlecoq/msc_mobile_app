package com.example.msc_mobile_app.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class OverviewPolyline(var points: String?)

@Serializable
data class Duration(
    var text: String?,
    var value: Int?
)

@Serializable
data class Distance(
    var text: String?,
    var value: Int?
)

@Serializable
data class DirectionApiResponse(
    @SerialName("geocoded_waypoints")
    var geocodedWaypoints: List<GeocodedWaypoint?>?,
    var routes: List<Route?>?,
    var status: String?
)

@Serializable
data class Bounds(
    var northeast: Northeast?,
    var southwest: Southwest?
)

@Serializable
data class Route(
    var bounds: Bounds?,
    var copyrights: String?,
    var legs: List<Leg?>?,
    @SerialName("overview_polyline")
    var overviewPolyline: OverviewPolyline?,
    var summary: String?,
    var warnings: List<String?>?, // Not sure if String
    @SerialName("waypoint_order")
    var waypointOrder: List<Int?>?
)

@Serializable
data class Polyline(
    var points: String?
)

@Serializable
data class Step(
    var distance: Distance?,
    var duration: Duration?,
    @SerialName("end_location")
    var endLocation: EndLocation?,
    @SerialName("html_instructions")
    var htmlInstructions: String?,
    var maneuver: String? = "",
    var polyline: Polyline?,
    @SerialName("start_location")
    var startLocation: StartLocation?,
    @SerialName("travel_mode")
    var travelMode: String?
)

@Serializable
data class Leg(
    var distance: Distance?,
    var duration: Duration?,
    @SerialName("end_address")
    var endAddress: String?,
    @SerialName("end_location")
    var endLocation: EndLocation?,
    @SerialName("start_address")
    var startAddress: String?,
    @SerialName("start_location")
    var startLocation: StartLocation?,
    var steps: List<Step?>?,
    @SerialName("traffic_speed_entry")
    var trafficSpeedEntry: List<String?>?, // Not sure if String
    @SerialName("via_waypoint")
    var viaWaypoint: List<String?>? // Not sure if String
)

@Serializable
data class GeocodedWaypoint(
    @SerialName("geocoder_status")
    var geocoderStatus: String?,
    @SerialName("place_id")
    var placeId: String?,
    var types: List<String?>?
)

@Serializable
data class Southwest(
    var lat: Double?,
    var lng: Double?
)

@Serializable
data class StartLocation(
    var lat: Double?,
    var lng: Double?
)

@Serializable
data class EndLocation(
    var lat: Double?,
    var lng: Double?
)

@Serializable
data class Northeast(
    var lat: Double?,
    var lng: Double?
)