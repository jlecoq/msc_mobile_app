package com.example.msc_mobile_app.models

import com.google.android.gms.maps.model.LatLng

enum class Type {PERSON, TRAIN_STATION, SCHOOL, BUS_STOP, UNIVERSITY_RESTAURANT}

data class Poi(val name: String, val type:Type, val location: LatLng)
