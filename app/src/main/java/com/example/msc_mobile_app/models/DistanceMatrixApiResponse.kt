package com.example.msc_mobile_app.models

import kotlinx.serialization.Serializable

@Serializable
data class Result(val value: Int, val text: String)

@Serializable
data class ElementResult(val status: String,
val duration: Result,
val distance: Result)

@Serializable
data class DistanceMatrixResult(val elements: Array<ElementResult>)

@Serializable
data class DistanceMatrixApiResponse(val status: String,
                                     val origin_addresses: Array<String>,
                                     val destination_addresses: Array<String>,
                                     val rows: Array<DistanceMatrixResult>)