package com.example.msc_mobile_app.APIs

// import com.example.msc_mobile_app.controllers.HttpController
import com.example.msc_mobile_app.keys.GOOGLE_API_KEY
import com.example.msc_mobile_app.models.DistanceMatrixApiResponse
import com.example.msc_mobile_app.models.Path
import com.example.msc_mobile_app.utils.LocationUtil
import com.google.android.gms.maps.model.LatLng
import io.ktor.client.*
import io.ktor.client.engine.android.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*

object DistanceMatrixApi {
    private val GOOGLE_DISTANCE_MATRIX_API = "https://maps.googleapis.com/maps/api/distancematrix/json?"
    private val client = HttpClient(Android) {
        install(JsonFeature) {
            serializer = KotlinxSerializer()
        }
    }

    private fun appendParams(params: Map<String, String>): String {
        var url = GOOGLE_DISTANCE_MATRIX_API
        val mapSize = params.size
        var i = 0

        params.forEach { key, value ->
            if (i == mapSize - 1) {
                url += "$key=$value"
            }
            else {
                url += "$key=$value&"
            }
            i++
        }

        return url
    }

    suspend fun get(origin: LatLng, destination: LatLng): DistanceMatrixApiResponse {
        var url = GOOGLE_DISTANCE_MATRIX_API
        url += "origins=${LocationUtil.toString(origin)}&destinations=${LocationUtil.toString(destination)}&key=${GOOGLE_API_KEY}"

        return client.get(url)
    }

    suspend fun path(origin: LatLng, destination: LatLng): Path {
        val result = get(origin, destination)
        val duration = result.rows[0].elements[0].duration.value
        val distance = result.rows[0].elements[0].distance.value
        return Path(duration, distance, null)
    }
}