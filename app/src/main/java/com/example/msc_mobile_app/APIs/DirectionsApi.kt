package com.example.msc_mobile_app.APIs

import android.util.Log
import com.example.msc_mobile_app.keys.GOOGLE_API_KEY
import com.example.msc_mobile_app.models.DirectionApiResponse
import com.example.msc_mobile_app.models.NodeLocations
import com.example.msc_mobile_app.utils.LocationUtil
import com.google.android.gms.maps.model.LatLng
import io.ktor.client.*
import io.ktor.client.engine.android.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*

object DirectionsApi {
    private val GOOGLE_DIRECTION_API = "https://maps.googleapis.com/maps/api/directions/json?"

    private val client = HttpClient(Android) {
        install(JsonFeature) {
            serializer = KotlinxSerializer()
        }
    }

//    suspend fun get(origin: LatLng, destination: LatLng): DirectionApiResponse {
//        var url = GOOGLE_DIRECTION_API
//        url += "origin=${LocationUtil.toString(origin)}&destination=${LocationUtil.toString(destination)}&key=$GOOGLE_API_KEY"
//        return client.get(url)
//    }

    suspend fun get(origin: LatLng, destination: LatLng, steps: NodeLocations): DirectionApiResponse {
        var url = GOOGLE_DIRECTION_API
        if (steps.size != 0) {
            url += "origin=${LocationUtil.toString(origin)}&destination=${LocationUtil.toString(destination)}&waypoints=${stepsToString(steps)}&key=$GOOGLE_API_KEY"
        }
        else {
            url += "origin=${LocationUtil.toString(origin)}&destination=${LocationUtil.toString(destination)}&key=$GOOGLE_API_KEY"
        }
        Log.e(javaClass.simpleName, url)
        return client.get(url)
    }

    private fun stepsToString(steps: NodeLocations): String {
        var str = ""
        for (i in steps.indices) {
            if (i == steps.size - 1) {
                str += LocationUtil.toString(steps[i])
            }
            else {
                str += LocationUtil.toString(steps[i]) + '|'
            }
        }

        return str
    }
}