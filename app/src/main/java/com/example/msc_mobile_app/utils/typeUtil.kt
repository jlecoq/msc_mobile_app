package com.example.msc_mobile_app.utils

typealias callback = () -> Unit
typealias Callback<T> = (T) -> Unit
typealias Callback3<Type1, Type2, Type3, ReturnType> = (Type1, Type2, Type3) -> ReturnType
typealias Callback2<Type1, Type2> = (Type1, Type2) -> Unit
typealias CallbackWithReceiver<T> = T.() -> Unit