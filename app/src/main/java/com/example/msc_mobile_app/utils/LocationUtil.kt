package com.example.msc_mobile_app.utils

import com.google.android.gms.maps.model.LatLng

object LocationUtil {
    fun toString(location: LatLng): String {
        return "${location.latitude},${location.longitude}"
    }
}