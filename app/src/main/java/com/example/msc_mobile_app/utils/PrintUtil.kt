package com.example.msc_mobile_app.utils

import android.util.Log

object PrintUtil {
    fun <T>printMatrix(matrix: Array<Array<T>>) {
        for (array in matrix) {
            Log.e(javaClass.simpleName, array.contentDeepToString())
        }
    }
}