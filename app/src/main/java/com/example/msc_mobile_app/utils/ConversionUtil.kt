package com.example.msc_mobile_app.utils

import kotlin.math.pow
import kotlin.math.roundToInt

object ConversionUtil {
    fun mToKm(meters: Int): Double {
        return (meters / 1000.0).roundTo(1)
    }
}

fun Double.roundTo(numFractionDigits: Int): Double {
    val factor = 10.0.pow(numFractionDigits.toDouble())
    return (this * factor).roundToInt() / factor
}
