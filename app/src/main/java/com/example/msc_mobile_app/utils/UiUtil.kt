package com.example.msc_mobile_app.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.DrawableCompat
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory

object UiUtil {
    lateinit var context: Context
    val VISUALIZE_MESSAGE = "VISUALIZE_MESSAGE"

    enum class VisualizeMessage(val str: String) {
        CONNECTED_GRAPH("CONNECTED_GRAPH"),
        TRANSITIVE_CLOSURE("TRANSITIVE_CLOSURE"),
        TRANSITIVE_CLOSURE_SHORTEST_DISTANCE("TRANSITIVE_CLOSURE_SHORTEST_DISTANCE"),
        MEET_AT_EARLIEST("MEET_AT_EARLIEST"),
        MEET_AT_CLOSEST("MEET_AT_SHORTEST")
    }

    fun drawableFromAssets(context: Context, fileName: String, onLoad: Callback<Drawable?>) {
        try {
            val inputStream = context.assets.open("image/${fileName}.png")
            val drawable = Drawable.createFromStream(inputStream, null)
            onLoad(drawable)
        } catch (e: Throwable) {
            onLoad(null)
        }
    }

    fun colorString(string: String, start: Int, end: Int, color: String): SpannableString {
        return colorString(string, start, end, Color.parseColor(color))
    }

    fun colorString(string: String, start: Int, end: Int, color: Int): SpannableString {
        return colorString(SpannableString(string), start, end, color)
    }

    fun colorString(span: SpannableString, start: Int, end: Int, color: Int): SpannableString {
        span.setSpan(
            ForegroundColorSpan(color),
            start,
            end,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        return span
    }

    /**
     * Demonstrates converting a [Drawable] to a [BitmapDescriptor],
     * for use as a marker icon.
     */
    fun vectorToBitmap(
        @DrawableRes id: Int,
        @ColorInt color: Int,
        width: Int? = null,
        height: Int? = null
    ): BitmapDescriptor {
        val vectorDrawable: Drawable =
            ResourcesCompat.getDrawable(context.resources, id, null)
                ?: return BitmapDescriptorFactory.defaultMarker()

        val iconWidth = width ?: vectorDrawable.intrinsicWidth
        val iconHeight = width ?: vectorDrawable.intrinsicHeight

        vectorDrawable.setBounds(0, 0, iconWidth, iconHeight)


        val bitmap = Bitmap.createBitmap(
            iconWidth,
            iconHeight,
            Bitmap.Config.ARGB_8888
        )

        val canvas = Canvas(bitmap)
        DrawableCompat.setTint(vectorDrawable, color)
        vectorDrawable.draw(canvas)

        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    enum class HexColor(val rgb: Long) {
        RED(0xffff0000),
        LIGHT_RED(0xaaff0000),
        GREEN(0xff32cd32),
        BLUE(0xff0000ff),
        LIGHT_BLUE(0x88000088),
        BLACK(0xff000000),
        DARK_WHITE(0x77ffffff),
        WHITE(0xddffffff),
        ORANGE(0xffff7f00),
        BROWN(0xff582900),
        GREY(0xff989898),
        PINK(0xffd993b1)
    }
}