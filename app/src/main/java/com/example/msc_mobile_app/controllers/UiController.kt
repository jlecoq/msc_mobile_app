package com.example.msc_mobile_app.controllers

import android.R
import android.app.AlertDialog
import android.content.Context
import android.util.ArrayMap
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import com.example.msc_mobile_app.utils.CallbackWithReceiver
import com.google.android.material.snackbar.Snackbar

object UiController {
    private var _activities: ArrayMap<Class<out AppCompatActivity>, in AppCompatActivity> = ArrayMap()
    private var _foregroundActivity: AppCompatActivity? = null
    lateinit var context: Context

    val activity
        get() = _foregroundActivity

    fun register(activity: AppCompatActivity) {
        _activities[activity.javaClass] = activity
        _foregroundActivity = activity
    }

    fun unregister(activity: AppCompatActivity) {
        val clazz = activity.javaClass
        if (_activities.contains(clazz)) {
            _activities.remove(clazz)
        }
        if (activity === _foregroundActivity) {
            _foregroundActivity = null
        }
    }

    fun withActivity(function: AppCompatActivity.() -> Unit) {
        _foregroundActivity?.apply(function)
    }

    private fun runOnUiThread(function: AppCompatActivity.() -> Unit) {
        _foregroundActivity?.apply {
            runOnUiThread {
                function(this)
            }
        }
    }

    fun showSnack(
        @StringRes message: Int,
        duration: Int = Snackbar.LENGTH_LONG,
        custom: CallbackWithReceiver<Snackbar>? = null
    ) {
        showSnack(context.getString(message), duration, custom)
    }

    fun showSnack(
        message: String,
        duration: Int = Snackbar.LENGTH_LONG,
        custom: (Snackbar.() -> Unit)? = null
    ) {
        runOnUiThread {
            val snack = Snackbar.make(findViewById(R.id.content), message, duration)
            if (custom != null) {
                custom(snack)
            }
            snack.show()
        }
    }

    fun showToast(@StringRes message: Int, duration: Int = Toast.LENGTH_LONG) {
        showToast(context.getString(message), duration)
    }

    fun showToast(message: String, duration: Int = Toast.LENGTH_LONG) {
        withActivity {
            Toast.makeText(this, message, duration).show()
        }
    }

    fun showADialog(
        @StringRes title: Int,
        @StringRes message: Int,
        custom: CallbackWithReceiver<AlertDialog.Builder>? = null
    ) {
        showADialog(context.getString(title), context.getString(message), custom)
    }

    fun showADialog(
        title: String?,
        message: String?,
        custom: CallbackWithReceiver<AlertDialog.Builder>? = null
    ) {
        runOnUiThread {
            val alertDialog = AlertDialog.Builder(this, R.style.Theme_DeviceDefault_Dialog_Alert)
            alertDialog.setTitle(title)
            alertDialog.setMessage(message)
            alertDialog.setCancelable(false)

            if (custom == null) {
                alertDialog.setNeutralButton(R.string.ok) { dialog, _ ->
                    dialog.dismiss()
                }
            } else {
                custom(alertDialog)
            }

            val alert: AlertDialog = alertDialog.create()

            alert.show()
        }
    }
}