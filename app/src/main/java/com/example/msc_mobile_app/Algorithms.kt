package com.example.msc_mobile_app

import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.example.msc_mobile_app.controllers.UiController
import com.example.msc_mobile_app.models.OriginDestinationGraph
import com.example.msc_mobile_app.utils.UiUtil
import com.google.android.material.bottomnavigation.BottomNavigationView

class Algorithms : BaseActivity() {
    private lateinit var computeMeetAtEarliestBtn: Button
    private lateinit var visualizeMeetAtEarliestBtn: Button

    private lateinit var computeMeetAtClosestBtn: Button
    private lateinit var visualizeMeetAtClosestBtn: Button

    private lateinit var buildConnectedGraphBtn: Button
    private lateinit var visualizeConnectedGraphBtn: Button

    private lateinit var buildTransitiveClosureBtn: Button
    private lateinit var visualizeTransitiveClosureBtn: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.activity_algorithms)

        loadBottomNavigationView()
        loadButtons()
        colorButtons()
    }

    private fun loadBottomNavigationView() {
        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottom_navigation)
        bottomNavigationView.selectedItemId = R.id.algorithms

        bottomNavigationView.setOnNavigationItemSelectedListener {
            when(it.itemId) {
                R.id.algorithms -> {
                }
                R.id.map -> {
                    startActivity(Intent(this, MapsActivity::class.java))
                    overridePendingTransition(0, 0)
                }
                else -> {
                }
            }
            true
        }
    }

    private fun loadButtons() {
        buildConnectedGraphBtn = findViewById(R.id.build_connected_graph_button)
        visualizeConnectedGraphBtn = findViewById(R.id.visualize_connected_graph_button)
        buildConnectedGraphBtn.setOnClickListener(::onBuildConnectedGraphBtn)
        visualizeConnectedGraphBtn.setOnClickListener(::onVisualizeConnectedGraphBtn)

        buildTransitiveClosureBtn = findViewById(R.id.build_transitive_closure_button)
        visualizeTransitiveClosureBtn = findViewById(R.id.visualize_transitive_closure_button)
        buildTransitiveClosureBtn.setOnClickListener(::onBuildTransitiveClosureBtn)
        visualizeTransitiveClosureBtn.setOnClickListener(::onVisualizeTransitiveClosure)

        computeMeetAtEarliestBtn = findViewById(R.id.compute_meet_at_earliest_button)
        visualizeMeetAtEarliestBtn = findViewById(R.id.visualize_meet_at_earliest_button)
        computeMeetAtEarliestBtn.setOnClickListener(::onComputeMeetAtEarliest)
        visualizeMeetAtEarliestBtn.setOnClickListener(::onVisualizeMeetAtEarliest)

        computeMeetAtClosestBtn = findViewById(R.id.compute_meet_at_closest_button)
        visualizeMeetAtClosestBtn = findViewById(R.id.visualize_meet_at_closest_button)
        computeMeetAtClosestBtn.setOnClickListener(::onComputeMeetAtClosest)
        visualizeMeetAtClosestBtn.setOnClickListener(::onVisualizeMeetAtClosest)
    }

    private fun color(button: Button, color: Int) {
        button.setBackgroundTintList(ColorStateList.valueOf(resources.getColor(color)))
    }

    private fun colorButtons() {
        if (!OriginDestinationGraph.connectedGraphDone) {
            color(buildConnectedGraphBtn, R.color.primary2)
            color(buildTransitiveClosureBtn, R.color.grey)
            color(computeMeetAtEarliestBtn, R.color.grey)
            color(computeMeetAtClosestBtn, R.color.grey)

            color(visualizeConnectedGraphBtn, R.color.grey)
            color(visualizeTransitiveClosureBtn, R.color.grey)
            color(visualizeMeetAtEarliestBtn, R.color.grey)
            color(visualizeMeetAtClosestBtn, R.color.grey)
        }
        else if (!OriginDestinationGraph.transitiveClosureDone) {
            color(buildTransitiveClosureBtn, R.color.primary2)
            color(visualizeConnectedGraphBtn, R.color.primary2)

            color(visualizeTransitiveClosureBtn, R.color.grey)
            color(computeMeetAtEarliestBtn, R.color.grey)
            color(computeMeetAtClosestBtn, R.color.grey)
            color(visualizeMeetAtEarliestBtn, R.color.grey)
            color(visualizeMeetAtClosestBtn, R.color.grey)
        } else {
            color(buildTransitiveClosureBtn, R.color.primary2)
            color(visualizeConnectedGraphBtn, R.color.primary2)
            color(visualizeTransitiveClosureBtn, R.color.primary2)
            color(computeMeetAtEarliestBtn, R.color.primary2)
            color(computeMeetAtClosestBtn, R.color.primary2)

            if (OriginDestinationGraph.computeMeetAtEarliestDone) {
                color(visualizeMeetAtEarliestBtn, R.color.primary2)
            }
            if (OriginDestinationGraph.computeMeetAtClosestDone) {
                color(visualizeMeetAtClosestBtn, R.color.primary2)
            }
        }
    }

    private fun onBuildConnectedGraphBtn(view: View) {
        OriginDestinationGraph.buildRandomConnectedGraph()
        color(visualizeConnectedGraphBtn, R.color.primary2)
        color(buildTransitiveClosureBtn, R.color.primary2)

        // TODO: to improve with if statement
        color(visualizeTransitiveClosureBtn, R.color.grey)
        color(computeMeetAtEarliestBtn, R.color.grey)
        color(computeMeetAtClosestBtn, R.color.grey)
        color(visualizeMeetAtEarliestBtn, R.color.grey)
        color(visualizeMeetAtClosestBtn, R.color.grey)
    }

    private fun onVisualizeConnectedGraphBtn(view: View) {
        if (!OriginDestinationGraph.connectedGraphDone) {
            UiController.showToast(
                "You first need to build the connected graph before visualizing it",
                Toast.LENGTH_SHORT
            )
            return
        }

        val intent = Intent(this, MapsActivity::class.java).apply {
            putExtra(UiUtil.VISUALIZE_MESSAGE, UiUtil.VisualizeMessage.CONNECTED_GRAPH.str)
        }
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

    private fun onBuildTransitiveClosureBtn(view: View) {
        if (!OriginDestinationGraph.connectedGraphDone) {
            UiController.showToast(
                "You first need to build the connected graph before building the transitive closure",
                Toast.LENGTH_SHORT
            )
            return
        }

        OriginDestinationGraph.buildTransitiveClosure()

        color(visualizeTransitiveClosureBtn, R.color.primary2)
        color(computeMeetAtEarliestBtn, R.color.primary2)
        color(computeMeetAtClosestBtn, R.color.primary2)
    }

    private fun onVisualizeTransitiveClosure(view: View) {
        if (!OriginDestinationGraph.connectedGraphDone) {
            UiController.showToast(
                "You first need to build the connected graph and build the transitive closure before visualizing it",
                Toast.LENGTH_SHORT
            )
            return
        }
        else if (!OriginDestinationGraph.transitiveClosureDone) {
            UiController.showToast(
                "You first need to build the transitive closure before visualizing it",
                Toast.LENGTH_SHORT
            )
            return
        }

        val intent = Intent(this, MapsActivity::class.java).apply {
            putExtra(UiUtil.VISUALIZE_MESSAGE, UiUtil.VisualizeMessage.TRANSITIVE_CLOSURE_SHORTEST_DISTANCE.str)
        }
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

    private fun onComputeMeetAtEarliest(view: View) {
        if (!OriginDestinationGraph.connectedGraphDone) {
            UiController.showToast(
                "You first need to build the connected graph and build the transitive closure before visualizing it",
                Toast.LENGTH_SHORT
            )
            return
        }
        else if (!OriginDestinationGraph.transitiveClosureDone) {
            UiController.showToast(
                "You first need to build the transitive closure before visualizing it",
                Toast.LENGTH_SHORT
            )
            return
        }

        OriginDestinationGraph.computeMeetAtEarliest()
        color(visualizeMeetAtEarliestBtn, R.color.primary2)
    }

    private fun onComputeMeetAtClosest(view: View) {
        if (!OriginDestinationGraph.connectedGraphDone) {
            UiController.showToast(
                "You first need to build the connected graph and build the transitive closure before visualizing it",
                Toast.LENGTH_SHORT
            )
            return
        }
        else if (!OriginDestinationGraph.transitiveClosureDone) {
            UiController.showToast(
                "You first need to build the transitive closure before visualizing it",
                Toast.LENGTH_SHORT
            )
            return
        }
        OriginDestinationGraph.computeMeetAtClosest()
        color(visualizeMeetAtClosestBtn, R.color.primary2)
    }

    private fun onVisualizeMeetAtEarliest(view: View) {
        if (!OriginDestinationGraph.connectedGraphDone) {
            UiController.showToast(
                "You first need to build the connected graph, build the transitive closure and compute \"meet at earliest\" before visualizing it",
                Toast.LENGTH_SHORT
            )
            return
        }
        else if (!OriginDestinationGraph.transitiveClosureDone) {
            UiController.showToast(
                "You first need to build the transitive closure, and compute \"meet at earliest\" before visualizing it",
                Toast.LENGTH_SHORT
            )
            return
        }
        else if (!OriginDestinationGraph.computeMeetAtEarliestDone) {
            UiController.showToast(
                "You first need to compute \"meet at earliest\" before visualizing it",
                Toast.LENGTH_SHORT
            )
            return
        }

        val intent = Intent(this, MapsActivity::class.java).apply {
            putExtra(UiUtil.VISUALIZE_MESSAGE, UiUtil.VisualizeMessage.MEET_AT_EARLIEST.str)
        }
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

    private fun onVisualizeMeetAtClosest(view: View) {
        if (!OriginDestinationGraph.connectedGraphDone) {
            UiController.showToast(
                "You first need to build the connected graph, build the transitive closure and compute \"meet at closest\" before visualizing it",
                Toast.LENGTH_SHORT
            )
            return
        }
        else if (!OriginDestinationGraph.transitiveClosureDone) {
            UiController.showToast(
                "You first need to build the transitive closure, and compute \"meet at closest\" before visualizing it",
                Toast.LENGTH_SHORT
            )
            return
        }
        else if (!OriginDestinationGraph.computeMeetAtClosestDone) {
            UiController.showToast(
                "You first need to compute \"meet at closest\" before visualizing it",
                Toast.LENGTH_SHORT
            )
            return
        }

        val intent = Intent(this, MapsActivity::class.java).apply {
            putExtra(UiUtil.VISUALIZE_MESSAGE, UiUtil.VisualizeMessage.MEET_AT_CLOSEST.str)
        }
        startActivity(intent)
        overridePendingTransition(0, 0)
    }
}
