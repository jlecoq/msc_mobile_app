package com.example.msc_mobile_app.extensions

inline fun <reified T> Array<Array<T>>.copy() = Array(size) { get(it).clone() }