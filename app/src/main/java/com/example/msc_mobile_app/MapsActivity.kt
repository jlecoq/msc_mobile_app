package com.example.msc_mobile_app

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import com.example.msc_mobile_app.APIs.DirectionsApi
import com.example.msc_mobile_app.models.*
import com.example.msc_mobile_app.utils.ConversionUtil
import com.example.msc_mobile_app.utils.UiUtil
import com.example.msc_mobile_app.utils.callback
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.maps.android.PolyUtil
import kotlinx.coroutines.*
import java.time.LocalTime
import java.time.format.DateTimeFormatter

class MapsActivity : BaseActivity(), OnMapReadyCallback {
    private lateinit var map: GoogleMap
    private var onMapLoaded: callback? = null
    private val indexToColor = arrayOf(UiUtil.HexColor.BLUE, UiUtil.HexColor.GREEN, UiUtil.HexColor.BROWN, UiUtil.HexColor.BLACK, UiUtil.HexColor.PINK)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.activity_maps)

        onMapLoaded = {
            // Get the Intent that started this activity and extract the string
            val message = intent.getStringExtra(UiUtil.VISUALIZE_MESSAGE)

            when (message) {
                UiUtil.VisualizeMessage.CONNECTED_GRAPH.str -> drawGraph(OriginDestinationGraph.connectedGraphMatrices)
                UiUtil.VisualizeMessage.TRANSITIVE_CLOSURE_SHORTEST_DISTANCE.str -> drawGraph(OriginDestinationGraph.meetAtClosestMatrices)
                UiUtil.VisualizeMessage.MEET_AT_CLOSEST.str -> {
                    drawPeopleItineraries(OriginDestinationGraph.nodesLocationsToMeetAtClosest)
                    showMeetInformation(OriginDestinationGraph.destinationDurationClosest, OriginDestinationGraph.destinationDistanceClosest)
                }
                UiUtil.VisualizeMessage.MEET_AT_EARLIEST.str -> {
                    drawPeopleItineraries(OriginDestinationGraph.nodesLocationsToMeetAtEarliest)
                    showMeetInformation(OriginDestinationGraph.destinationDurationEarliest, OriginDestinationGraph.destinationDistanceEarliest)
                }
            }
        }

        loadBottomNavigationView()
        loadGraphIfFirstRun()

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map_view) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun showMeetInformation(duration: Int, distance: Int) {
        val meet_information_view = findViewById<FrameLayout>(R.id.meet_information)
        val meet_information_text = findViewById<TextView>(R.id.meet_information_text)
        meet_information_view.visibility = View.VISIBLE

        val arrivalTime = LocalTime.now().plusSeconds(duration.toLong())
        val lowBoundArrivalTime = arrivalTime.minusMinutes(5)
        val upBoundArrivalTime = arrivalTime.plusMinutes(5)

        val formater = DateTimeFormatter.ofPattern("HH:mm")
        val lowBoundArrivalTimeStr = lowBoundArrivalTime.format(formater)
        val upBoundArrivalTimeStr = upBoundArrivalTime.format(formater)

        val distance = ConversionUtil.mToKm(distance)

        meet_information_text.text = "Meet at ${OriginDestinationGraph.destination.name} around [$lowBoundArrivalTimeStr - $upBoundArrivalTimeStr] ($distance km)"
    }

    private fun indexToColor(index: Int): UiUtil.HexColor {
        return indexToColor[index % indexToColor.size]
    }

    private fun drawPeopleItineraries(itineraries: NodesLocations) {
        runBlocking {
            for ((studentIndex, itinerary) in itineraries) {
                launch {
                    val response =  DirectionsApi.get(itinerary.origin, itinerary.destination, itinerary.steps)
                    val color = indexToColor(studentIndex)
                    drawDirectionRoute(response, color)
                }
            }
        }
    }

    private fun loadBottomNavigationView() {
        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottom_navigation)
        bottomNavigationView.selectedItemId = R.id.map
        bottomNavigationView.setOnNavigationItemSelectedListener {
            when(it.itemId) {
                R.id.algorithms -> {
                    startActivity(Intent(this, Algorithms::class.java))
                    overridePendingTransition(0, 0)
                }
                R.id.map -> {
                }
                else -> {
                }
            }

            true
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        OriginDestinationGraph.pointOfInterests.forEach {
            addMarker(it)
        }

        val peopleNommay = Poi("People Nommay", Type.PERSON, LatLng(47.5390766247414, 6.850901578342239))

        val cameraPosition = CameraPosition.builder()
            .target(peopleNommay.location)
            .zoom(11f)
            .build()
        map.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        map.setOnMarkerClickListener {
            Log.e(javaClass.simpleName, it.position.toString())
            true
        }

        onMapLoaded?.invoke()
    }

    private fun addMarker(poi: Poi) {
        val color: Int
        val picture: Int
        val markerOptions = MarkerOptions()
            .position(poi.location)
            .title(poi.name)
            .anchor(0.5f, 0.5f)
            .zIndex(1f)

        when (poi.type) {
            Type.PERSON -> {
                val studentIndex = OriginDestinationGraph.pointOfInterests.indexOf(poi)
                color = indexToColor(studentIndex).rgb.toInt()
                picture = R.drawable.ic_pin_person
                markerOptions.draggable(true)
            }
            Type.BUS_STOP -> {
                color = UiUtil.HexColor.GREEN.rgb.toInt()
                picture = R.drawable.ic_bus_stop
            }
            Type.SCHOOL -> {
                color = UiUtil.HexColor.ORANGE.rgb.toInt()
                picture = R.drawable.ic_school
            }
            Type.TRAIN_STATION -> {
                color = UiUtil.HexColor.BROWN.rgb.toInt()
                picture = R.drawable.ic_train_station
            }
            Type.UNIVERSITY_RESTAURANT -> {
                color = UiUtil.HexColor.PINK.rgb.toInt()
                picture = R.drawable.ic_restaurant
            }
        }

        val icon = UiUtil.vectorToBitmap(
            picture,
            color,
            90,
            90
        )

        markerOptions.icon(icon)
        map.addMarker(markerOptions)
    }

    private fun drawItinerary(itinerary: Array<LatLng>, color: UiUtil.HexColor = UiUtil.HexColor.RED) {
        val polylineOptions = PolylineOptions().color(color.rgb.toInt())

        for (coordinates in itinerary) {
            polylineOptions.add(coordinates)
        }

        map.addPolyline(polylineOptions)
    }

    private fun <T> drawGraph(matrix: Matrix<T?>) {
        for (i in matrix.indices) {
            for (j in matrix.indices) {
                if (matrix[i][j] != null) {
                    val node1 = OriginDestinationGraph.pointOfInterests[i]
                    val node2 = OriginDestinationGraph.pointOfInterests[j]
                    val nodePositions = arrayOf(node1.location, node2.location)

                    if (node1.type == Type.PERSON) {
                        val studentIndex = OriginDestinationGraph.pointOfInterests.indexOf(node1)
                        val color = indexToColor(studentIndex)
                        drawItinerary(nodePositions, color)
                    }
                    else if (node2.type == Type.PERSON) {
                        val studentIndex = OriginDestinationGraph.pointOfInterests.indexOf(node2)
                        val color = indexToColor(studentIndex)
                        drawItinerary(nodePositions, color)
                    }
                    else {
                        drawItinerary(nodePositions)
                    }
                }
            }
        }
    }

    private fun loadGraphIfFirstRun() {
        if (firstRun) {
            firstRun = false

            val montbeliardStation = Poi("Montbéliard train station", Type.TRAIN_STATION, LatLng(47.51057684986036, 6.8014701058157705))
            val belfortStation = Poi("Belfort train station", Type.TRAIN_STATION, LatLng(47.6345195437623, 6.853778713980302))
            val hericourtStation = Poi("Hericourt train station", Type.TRAIN_STATION, LatLng(47.57314639608108, 6.769499358158861))
            val peopleNommay = Poi("People Nommay", Type.PERSON, LatLng(47.5390766247414, 6.850901578342239))
            val belfortUtbm = Poi("UTBM Belfort", Type.SCHOOL, LatLng(47.64243264582627, 6.844865301023619))
            val belfortIut = Poi("Iut Belfort", Type.SCHOOL, LatLng(47.643047095576335, 6.839095848661205))
            val busStopDonzelot = Poi("Bus stop Donzelot", Type.BUS_STOP, LatLng(47.496283537126, 6.808183964689486))
            val busStopHelvétie = Poi("Bus stop Helvétie", Type.BUS_STOP, LatLng(47.508424151884306, 6.805694149725605))
            val busStopAcropole = Poi("Bus stop Acropole", Type.BUS_STOP, LatLng(47.50949329755598, 6.8020234127703905))
            val ruMontbéliard = Poi("RU Montbéliard", Type.UNIVERSITY_RESTAURANT, LatLng(47.495971295531916, 6.803655424426408))
            val buildingAUfrStgi = Poi("Building A - UFR STGI", Type.SCHOOL, LatLng(47.49534272145077, 6.804209306743831))
            val buildingBUfrStgi = Poi("Building B - UFR STGI", Type.SCHOOL, LatLng(47.49522180245093, 6.8047597420840376))
            val buildingCUfrStgi = Poi("Building C - UFR STGI", Type.SCHOOL, LatLng(47.49550022057079, 6.804631908740001))
            val peopleBeaucourt = Poi("People Beaucourt - UFR STGI", Type.PERSON, LatLng(47.484066036147816, 6.923864051138618))
            val peopleFeschesLeChatel = Poi("People Fesches-le-Châtel - UFR STGI", Type.PERSON, LatLng(47.523271669968146, 6.908718874887538))

            val pois = arrayOf(belfortStation, montbeliardStation, hericourtStation, peopleFeschesLeChatel, peopleNommay, peopleBeaucourt, belfortUtbm, belfortIut, busStopAcropole, busStopDonzelot, busStopHelvétie, ruMontbéliard, buildingAUfrStgi, buildingBUfrStgi, buildingCUfrStgi)
            OriginDestinationGraph.init(pois)
        }
    }

    private fun drawDirectionRoute(response: DirectionApiResponse, color: UiUtil.HexColor = UiUtil.HexColor.BLUE) {
        val shape = response.routes?.get(0)?.overviewPolyline?.points
        val polyline = PolylineOptions()
            .addAll(PolyUtil.decode(shape))
            .color(color.rgb.toInt())
        map.addPolyline(polyline)
    }

    override fun onStop() {
        onMapLoaded = null

        super.onStop()
    }

    companion object {
        var firstRun = true
    }
}