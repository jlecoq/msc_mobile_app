package com.example.msc_mobile_app

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.example.msc_mobile_app.controllers.UiController
import com.example.msc_mobile_app.utils.UiUtil
import com.google.android.gms.maps.model.LatLng
import java.util.ArrayList

abstract class BaseActivity: AppCompatActivity() {
    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(newBase)

        newBase?.let {
            UiUtil.context = it
            UiController.context = it
        }
    }

    override fun onStart() {
        super.onStart()
        UiController.register(this)
    }

    override fun onResume() {
        super.onResume()
        UiController.register(this)
    }

    override fun onPause() {
        super.onPause()
        UiController.unregister(this)
    }

    override fun onStop() {
        super.onStop()
        UiController.unregister(this)
    }

    override fun onDestroy() {
        UiController.unregister(this)
        super.onDestroy()

    }
}